#pragma once
#include<iostream>
#include<string>
#include<fstream>
#include<algorithm>

class Carte {
	std::string m_titlu;
	std::string *m_autori;
	int m_numarAutori, m_cantitate;
	double m_pret;

public:
	Carte();
	Carte(const std::string, const std::string *, const int, const double, const int);
	Carte(const Carte&);
	Carte& operator=(const Carte&);
	~Carte();

	friend std::istream& operator>>(std::istream&, Carte&);
	friend std::ostream& operator<<(std::ostream&, const Carte&);

	Carte& operator+(int);
	void operator++();
	bool operator==(const Carte&);
	bool operator<(const Carte&);
	int getCantitate();
};
