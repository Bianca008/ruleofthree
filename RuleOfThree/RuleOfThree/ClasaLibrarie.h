#pragma once
#include "ClasaCarte.h"

class Librarie {
	std::string m_locatie;
	Carte *m_inventar;
	int m_nrCarti;

public:
	Librarie(std::string locatie = "undefined", int nr = 0);
	Librarie& operator=(const Librarie&);
	Librarie(const Librarie&);
	~Librarie();

	friend std::istream& operator>>(std::istream&, Librarie&);
	friend std::ostream& operator<<(std::ostream&, const Librarie&);

	friend Librarie adaugaCarti(Librarie);
	void sortareDupaPret();
	int distincte(const Librarie&);
	bool unicat(const Carte&, int&);
	friend Librarie mergeCarti(Librarie&, const Librarie&);
};