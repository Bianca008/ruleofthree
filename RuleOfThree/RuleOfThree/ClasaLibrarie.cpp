#include "ClasaLibrarie.h"

Librarie::Librarie(std::string locatie, int nr) {
	if (m_locatie.empty())
		m_locatie.clear();
	m_inventar = nullptr;
	m_locatie = locatie;
	m_nrCarti = nr;
	m_inventar = new Carte[m_nrCarti];
}

Librarie::~Librarie() {
	if (m_locatie.empty())
		m_locatie.clear();
	delete[]m_inventar;
	m_nrCarti = 0;
}

std::istream& operator>>(std::istream& in, Librarie& lib) {
	if (lib.m_locatie.empty())
		lib.m_locatie.clear();
	in >> lib.m_locatie;
	in >> lib.m_nrCarti;
	lib.m_inventar = new Carte[lib.m_nrCarti];
	for (int i = 0; i < lib.m_nrCarti; i++)
		in >> lib.m_inventar[i];
	return in;
}

std::ostream& operator<<(std::ostream& out, const Librarie& lib) {
	out << "Locatia: " << lib.m_locatie << "\n";
	out << "Carti:\n";
	for (int i = 0; i < lib.m_nrCarti; i++)
		out << lib.m_inventar[i];
	out << "\n";
	return out;
}

Librarie::Librarie(const Librarie& lib) {
	this->m_locatie = lib.m_locatie;
	this->m_nrCarti = lib.m_nrCarti;
	m_inventar = new Carte[m_nrCarti];
	for (int i = 0; i < m_nrCarti; i++)
		this->m_inventar[i] = lib.m_inventar[i];
}

Librarie& Librarie::operator=(const Librarie& lib) {
	this->m_locatie = lib.m_locatie;
	this->m_nrCarti = lib.m_nrCarti;
	m_inventar = new Carte[m_nrCarti];
	for (int i = 0; i < m_nrCarti; i++)
		this->m_inventar[i] = lib.m_inventar[i];
	return *this;
}

Librarie adaugaCarti(Librarie lib) {
	int nr;
	std::cout << "Nr. de carti pe care doriti sa le adaugati: ";
	std::cin >> nr;
	Carte car;

	Librarie libNoua(lib.m_locatie, nr + lib.m_nrCarti);
	libNoua.m_locatie = lib.m_locatie;
	libNoua.m_inventar = new Carte[nr + lib.m_nrCarti];
	libNoua.m_nrCarti = nr + lib.m_nrCarti;
	for (int i = 0; i < lib.m_nrCarti; i++)
		libNoua.m_inventar[i] = lib.m_inventar[i];

	for (int i = lib.m_nrCarti; i < lib.m_nrCarti + nr; i++) {
		std::cin >> libNoua.m_inventar[i];
	}
	return libNoua;
}

void Librarie::sortareDupaPret() {
	std::sort(m_inventar, m_inventar + m_nrCarti,
		[](Carte&a, const Carte&b)->bool {
		return a < b;
	});
}

int Librarie::distincte(const Librarie& lib) {
	int nr = lib.m_nrCarti + m_nrCarti;;
	for (int i = 0; i < m_nrCarti; i++)
		for (int j = 0; j < lib.m_nrCarti; j++)
			if (lib.m_inventar[j] == m_inventar[i])
				nr--;
	return nr;
}

bool Librarie::unicat(const Carte& car, int &poz) {
	for (int i = 0; i < m_nrCarti; i++)
		if (m_inventar[i] == car) {
			poz = i;
			return false;
		}
	return true;
}

Librarie mergeCarti(Librarie& lib1, const Librarie& lib2) {
	Librarie libMerge;
	int poz;
	int nr = lib1.distincte(lib2);
	libMerge.m_inventar = new Carte[nr];
	libMerge.m_nrCarti = nr;
	for (int i = 0; i < lib1.m_nrCarti; i++)
		libMerge.m_inventar[i] = lib1.m_inventar[i];
	int contor = lib1.m_nrCarti;
	for (int i = 0; i < lib2.m_nrCarti; i++) {
		poz = 0;
		if (lib1.unicat(lib2.m_inventar[i], poz) == true)
			libMerge.m_inventar[contor++] = lib2.m_inventar[i];
		else
			libMerge.m_inventar[poz] = libMerge.m_inventar[poz] + lib2.m_inventar[i].getCantitate();
	}
	return libMerge;
}