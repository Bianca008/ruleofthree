#include "ClasaCarte.h"

Carte::Carte() {
	m_titlu = "undefined";
	m_autori = nullptr;
	m_numarAutori = 0;
	m_cantitate = 0;
	m_pret = 0;
}

Carte::Carte(const std::string titlu, const std::string *autori, const int numarAutori, const double pret, const int cantitate) {
	if (m_titlu.empty())
		m_titlu.clear();
	m_titlu = titlu;
	m_numarAutori = numarAutori;
	m_autori = new std::string[m_numarAutori];
	for (int i = 0; i < m_numarAutori; i++)
		m_autori[i] = autori[i];
	m_pret = pret;
	m_cantitate = cantitate;
}

Carte::Carte(const Carte& copie) {
	this->m_titlu = copie.m_titlu;
	this->m_numarAutori = copie.m_numarAutori;
	this->m_autori = new std::string[m_numarAutori];
	for (int i = 0; i < m_numarAutori; i++)
		this->m_autori[i] = copie.m_autori[i];
	this->m_pret = copie.m_pret;
	this->m_cantitate = copie.m_cantitate;
}

Carte& Carte::operator=(const Carte& copie) {
	this->m_titlu = copie.m_titlu;
	this->m_numarAutori = copie.m_numarAutori;
	this->m_autori = new std::string[m_numarAutori];
	for (int i = 0; i < m_numarAutori; i++)
		this->m_autori[i] = copie.m_autori[i];
	this->m_pret = copie.m_pret;
	this->m_cantitate = copie.m_cantitate;
	return *this;
}

Carte::~Carte() {
	if (m_titlu.empty())
		m_titlu.clear();
	if (m_autori != nullptr)
		delete[] m_autori;
	m_numarAutori = 0;
	m_cantitate = 0;
	m_pret = 0;
}

std::istream& operator>>(std::istream& in, Carte& car) {
	in >> car.m_titlu >> car.m_numarAutori;
	car.m_autori = new std::string[car.m_numarAutori];
	for (int i = 0; i < car.m_numarAutori; i++)
		in >> car.m_autori[i];
	in >> car.m_pret >> car.m_cantitate;
	return in;
}

std::ostream& operator<<(std::ostream& out, const Carte& car) {
	out << "Titlu: " << car.m_titlu << "\n";
	out << "Nr. autori: " << car.m_numarAutori << "\n";
	out << "Autori:\n";
	for (int i = 0; i < car.m_numarAutori; i++)
		out << car.m_autori[i] << "\n";
	out << "Pret: " << car.m_pret << "\n";
	out << "Cantitate: " << car.m_cantitate << "\n\n";
	return out;
}

Carte& Carte::operator+(int adaug) {
	m_cantitate = m_cantitate + adaug;
	return *this;
}

void Carte :: operator++() {
	m_cantitate++;
}

bool Carte::operator==(const Carte& car) {
	if (this->m_titlu.compare(car.m_titlu) != 0)
		return false;
	if (m_numarAutori != car.m_numarAutori)
		return false;
	for (int i = 0; i < m_numarAutori; i++)
		if (this->m_autori[i].compare(car.m_autori[i]) != 0)
			return false;
	return true;
}

bool Carte::operator<(const Carte& car) {
	return this->m_pret < car.m_pret;
}

int Carte::getCantitate() {
	return m_cantitate;
}