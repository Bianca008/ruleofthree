#include "ClasaCarte.h"
#include "ClasaLibrarie.h"

void citire(Librarie &lib1, Librarie &lib2) {
	std::ifstream f("fisCarti.txt");
	f >> lib1 >> lib2;
	f.close();
}

int main()
{
	Librarie lib1, lib2;

	citire(lib1, lib2);
	std::cout << lib1;
	std::cout << lib2;

	lib1.sortareDupaPret();
	std::cout << lib1;

	std::cout << "Merge: \n";
	Librarie lib3;
	lib3 = mergeCarti(lib1, lib2);
	//lib3 = adaugaCarti(lib1);
	std::cout << lib3;

	system("pause");
	return 0;
}